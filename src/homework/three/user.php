<?php
/**
 * Created by PhpStorm.
 * User: Jekyn
 * Date: 18.06.2017
 * Time: 22:08
 */

namespace homework\three;
/**
 * �����, �������������� �������
 * @package homework\dz3
 */

class user
{
    private $login;
    private $password;
    private $role;
    private $name;
    private $secname;
    private $fathername;
    private $old;
    private $birthday;
    public function __construct($new_user){
        $json_string=file_get_contents( $new_user ,true);//��������� ���� �� ���� ������� �������� ���
        $user=json_decode($json_string,true);        //��������� ������ �����
        $this->login = $user['login'];        //� ��������� ������
        $this->password = $user['password'];
        $this->role = $user['role'];
        $this->name = $user['name'];
        $this->secname = $user['second_name'];
        $this->fathername = $user['father_name'];
        $this->old = $user['old'];
        $this->birthday = $user['birthday'];


    }
    public function get_login()
    {
        return $this->login;
    }
    public function get_password()
    {
        return  $this->password ;
    }
    public function get_role()
    {
        return  $this->role  ;
    }
    public function get_name()
    {
        return  $this->name  ;
    }
    public function get_secname()
    {
        return  $this->secname ;
    }
    public function get_fathername()
    {
        return  $this->fathername;
    }
    public function get_old()
    {
        return  $this->old;
    }
    public function get_birthday()
    {
        return  $this->birthday;
    }
}